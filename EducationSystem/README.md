Education System

简介
• 远程教育和多屏幕协作是智能教育的重要场景。
• 在本文中，CodeLab使用亲子早期教育系统来完成两个有关分布式早期教育算术问题和分布式益智游戏的综合案例。
• 它旨在帮助开发人员快速了解HarmonyOS应用程序开发，多屏协作交互和分布式跨设备传输的经验。
• 从项目创建、代码编写到编译、构造、部署和操作。


安装要求
• 安装DevEco Studio和Node.js
• 设置DevEco Studio开发环境。 DevEco Studio开发环境需要连接到网络，以确保该正常使用。可以根据以下两种情况配置开发环境：
    1.如果您可以直接访问Internet，则只需下载HarmonyOS SDK
    2.如果网络无法直接访问Internet，则可以通过代理服务器进行访问
• 生成密钥并申请证书

用户指南
• 下载此项目
• 打开HUAWEI DevEco Studio，单击File> Open选择此ComponentCodelab
• 单击Build> Build App(s)/Hap(s)>Build Debug Hap(s)以编译hap软件包
• 单击Run> Run 'entry'以运行hap包

注意
• 您可以选择在模拟器或真机上运行hap软件包。 
• 如果在真机上运行它，则需要在项目的File> Project Structure> Modules> Signing Configs中配置签名和证书信息。
 
许可
请参阅LICENSE文件以获得更多信息。