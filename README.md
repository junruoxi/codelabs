# codelabs
该Codelabs旨在向开发人员展示如何通过趣味场景来展示如何使用HarmonyOS能力的示例应用程序文档教程。
注意：IDE版本及配套SDK问题可前往https://devecostudio.rnd.huawei.com/download/aide/#download 查看配套的IDE、鸿蒙SDK、插件及Gradle版本。

ComponentCodelab:展示JAVA通用组件用法，链接：https://gitee.com/openharmony/codelabs/tree/master/ComponentCodelab

SimpleVideoCodelab：使用视频接口能力实现一个简单的视频播放器，链接：https://gitee.com/openharmony/codelabs/tree/master/SimpleVideoCodelab

DistributedVideoCodelab：如何使用分布式能力进行视频跨设备播放及控制，链接：https://gitee.com/openharmony/codelabs/tree/master/DistributedVideoCodelab

EducationSystem：展示了分布式的早教算术题和拼图游戏的案例，链接：https://gitee.com/openharmony/codelabs/tree/master/EducationSystem

HarmonyOSNewsClient：展示了一个可以分享新闻详情页面的简易新闻客户端，链接：https://gitee.com/openharmony/codelabs/tree/master/HarmonyOSNewsClient

DistributedMail：展示了一个可以跨设备迁移和分布式文件调用的简易邮件编辑页面，链接：https://gitee.com/openharmony/codelabs/tree/master/DistributedMail

ShoppingJs：展示了一个使用JS UI组件开发的购物应用，链接：https://gitee.com/openharmony/codelabs/tree/master/ShoppingJs

VoiceCamera：展示了一个基于分布式文件系统和AI语音识别功能开发的分布式语音照相机，链接：https://gitee.com/openharmony/codelabs/tree/master/VoiceCamera