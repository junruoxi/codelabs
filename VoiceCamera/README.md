# 分布式语音照相机

简介
在本Codelab中，使用了HarmonyOS分布式文件系统和AI语音识别功能来开发分布式语音照相机。
使用此相机应用程序，同一分布式网络下的不同设备可以实时查看主设备拍摄的照片。
这有效地解决了彼此拍照时需要来回传递手机的麻烦。
此外，主设备还支持语音控制​​摄像头功能，可让您从远处控制摄像头。

许可
请参阅LICENSE文件以获得更多信息。